#   HW3PROBPLOTS Driver for Julia 0.3 code to run gradient descent and Newton for Hw 3 Problem 1
#  
#   Reference:
#   Nocedal and Wright, Numerical Optimization, 2nd Ed. Springer 2006
#
#   See also HW3PROB1
#

PARTFLAG = 3
ROFFLAG = 1
SDFLAG = 0
PDFLAG = 1
if PARTFLAG == 3
    
    if SDFLAG == 1
        num1 = 8
        xl = fill(zeros(2),num1)
        itsmax = 100
        incr = (1/(num1-1))*[pi;10*pi]
        for k= 1:num1
            xl[k] = [-pi/2;-10*(pi/2)]+(k-1)*incr+[1E-10;1E-10]
        end

    elseif PDFLAG == 1
        num1 = 5
        xl = fill(zeros(2),num1)
        itsmax = 10
        # #pd points:
        xl[1] = [0,0.1] 
        xl[2] = [-pi/8,-10*pi/8]
        #nd:
        xl[3] = [0.5,11] #not nd enough, neg ev close to 0
        xl[4] = [0.78,15]
        xl[5] = [pi/2,10*pi/2]
       
    else
        num1 = 5
        xl = fill(zeros(2),num1)
        itsmax = 20
        if ROFFLAG == 1
            pwr = 0.00005
            for i = 1:num1
                xl[i] = [(i-1)*pwr+1.1655,0]
            end
            # part 1)e)iii
        elseif ROFFLAG == 2
            # radius for the y coordinate
            pwr = 0.0005
            for i = 1:num1
                xl[i] = [0,(i-1)*pwr+11.655]
            end
            #
            #find the radius of convergence
        elseif ROFFLAG == 3
            pwr = 0.00005
            for i = 1:num1
                xl[i] = [(i-1)*pwr+1.2511,(i-1)*pwr+1.2511]
            end
        else
        end
    end

    
    epstol = 1e-20
    include("hw3prob1.jl")
    close("all")
    
    # part i,ii
    strs = Array(String,1,num1)
    for i = 0:(num1-1)
        strs[i+1] = "+0+$(i*550)"
    end
    mypath = "/h1/ellenle/Desktop/hwplots"
    xexact = [0;0]

    if (SDFLAG == 1) | (PDFLAG==1)
        figure(figsize=(14,25))
        plt[:get_current_fig_manager]()[:window][:wm_geometry](strs[1])   
    end
    
    for i = 1:num1
        fignum = i
        x0 = xl[fignum]
        
        if SDFLAG == 1
            # # sd
            its,x,err = sd_or_newton(x0,xexact,g,p1sd_dir!,epstol,itsmax,dolinesearch=false)
            itsls,xls,errls = sd_or_newton(x0,xexact,g,p1sd_dir!,epstol,itsmax)

        else
            # Newton
            itsn,xn,errn = sd_or_newton(x0,xexact,g,p1n_dir!,epstol,itsmax,dolinesearch=false)
            #@show(xn)
            itslsn,xlsn,errlsn = sd_or_newton(x0,xexact,g,p1n_dir!,epstol,itsmax)
        end

        
        if (SDFLAG == 1) | (PDFLAG==1)
        else
            figure(figsize=(14,6))   
            plt[:get_current_fig_manager]()[:window][:wm_geometry](strs[i])
        end

        if SDFLAG == 1
            # # sd
            subplot(4,2,i)
            plot(err,label="error SD",linewidth=2, marker="o",markersize=4)
            plot(errls,label="error SD with linesearch",linewidth=2,marker="o",markersize=5)
            xlabel("\$k\$, \$x_0\$ = $(x0)",fontsize=15) 
            ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
            legend(loc="best")
            savefig(mypath*"/hw2f$(i).png")
        elseif PDFLAG == 1
      # Newton
            subplot(3,2,i)
            plot(errlsn,label="error Newton with linesearch",linewidth=2,marker="o",markersize=5)
            legend(loc="best")
            xlabel("iteration \$k\$, \$x_0\$ = $(x0)",fontsize=15) 
            ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
            plot(errn,label="error Newton",linewidth=2, marker="o",markersize=4)    
            #@show xn    
            legend(loc="best")
            xlabel("iteration \$k\$, \$x_0\$ = $(x0)",fontsize=15) 
            ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
        else 
            # Newton
            subplot(1,2,1)
            plot(errlsn,label="error Newton with linesearch",linewidth=2,marker="o",markersize=5)
            legend(loc="best")
            xlabel("iteration \$k\$, \$x_0\$ = $(x0)",fontsize=15) 
            ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
            subplot(1,2,2)
            plot(errn,label="error Newton",linewidth=2, marker="o",markersize=4)    
            #@show xn    
            legend(loc="best")
            xlabel("iteration \$k\$, \$x_0\$ = $(x0)",fontsize=15) 
            ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
            savefig(mypath*"/hw2fn$(i).png")       
        end
    end
end

if SDFLAG == 1
    savefig(mypath*"/hw2sd.png")
elseif PDFLAG ==1
    savefig(mypath*"/hw2pd.png")
end


if PARTFLAG == 4
    # Show convergence rate is quadratic?
    x0 = [0,11.655]
    itsn,xn,errn = sd_or_newton(x0,xexact,g,p1n_dir!,epstol,itsmax,dolinesearch=false)
    itslsn,xlsn,errlsn = sd_or_newton(x0,xexact,g,p1n_dir!,epstol,itsmax)
    figure(figsize=(21,6))
    plt[:get_current_fig_manager]()[:window][:wm_geometry]("+0+0")
    subplot(1,2,1)
    plot(log(errn[1:end-1]),log(errn[2:end]),label="Newton without linesearch",
         linewidth=2,marker="o",markersize=5)
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    legend(loc=2)
    y = log(errn)


    x1 = y[9]
    y1 = y[10]
    x2 = y[7]
    y2 = y[8]
    m = (y2-y1)/(x2-x1)
    @show m
    h(z) = m.*(z-x1) + y1
    z = linspace(y[9],y[1],20)
    subplot(1,2,2)
    plot(log(errn[1:end-1]),log(errn[2:end]),label="Newton without linesearch",
         linewidth=2,marker="o",markersize=5)
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    plot(z,h(z),label="linear approxim. with slope m=$(m)",linewidth=1,marker="o",markersize=5)
    legend(loc=2)
    savefig(mypath*"/hw2convn.png")

    figure(figsize=(21,6))
    plt[:get_current_fig_manager]()[:window][:wm_geometry]("+0+0")
    subplot(1,2,1)
    plot(log(errlsn[1:end-1]),log(errlsn[2:end]),label="Newton w/ linesearch",
         linewidth=2,marker="o",markersize=5)
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    legend(loc=2)
    p = log(errlsn)
    x1 = p[9]
    p1 = p[10]
    x2 = p[7]
    p2 = p[8]
    m = (p2-p1)/(x2-x1)
    @show m
    h2(z) = m.*(z-x1) + p1
    z = linspace(p[9],p[1],20)
    subplot(1,2,2)
    plot(log(errlsn[1:end-1]),log(errlsn[2:end]),label="Newton w/
         linesearch",linewidth=2,marker="o",markersize=5)
    @show(log(errlsn),log(errn))
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    plot(z,h2(z),label="linear approxim. with slope m=$(m)",linewidth=1,marker="o",markersize=5)
    legend(loc=2)
    savefig(mypath*"/hw2convlsn.png")
    
    its,x,err =
        sd_or_newton(x0,xexact,g,p1sd_dir!,epstol,itsmax)
    v = log(err)
    x1 = v[end-1]
    y1 = v[end]
    m = (p2-p1)/(x2-x1)
    @show m
    h3(z) = 1.*(z-x1) + y1
    z = linspace(v[end-1],v[1],20)
    figure(figsize=(10,6))
    plt[:get_current_fig_manager]()[:window][:wm_geometry]("+0+0")
    plot(log(err[1:end-1]),log(err[2:end]),label="SD with linesearch",linewidth=2,marker="o",markersize=5)
    plot(z,h3(z),label="linear approxim. with slope m=1",linewidth=1,marker="o",markersize=5)
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    legend(loc=2)
end
