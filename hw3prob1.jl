#HW3PROB1  Julia 0.3 code to run gradient descent and Newton for Hw 3 Problem 1
#  
#   include("deconv2D.jl")  plots 3 figures: 1)
#
#   Reference:
#   Nocedal and Wright, Numerical Optimization, 2nd Ed. Springer 2006
#
#   See also 
#
#   Omar Ghattas 2015, modified by Ellen Le 2015
using Debug #needs to be called first
using PyPlot
using Optim

function f(x::Vector)
 -cos(x[1])*cos(x[2]/10)
end

function g(x::Vector)
    y = zeros(2)
    y[1] = (sin(x[1]))*(cos(x[2]/10))
    y[2] = (1/10)*(cos(x[1])*(sin(x[2]/10)))
    return y
end

function p1sd_dir!(x::Vector, storage::Vector)
    grad = g(x)
    storage[1] = -grad[1]
    storage[2] = -grad[2]
end

# computes p = -inv(H)*grad
function p1n_dir!(x::Vector, storage::Vector)
    y = x[2]    
    z = x[1]
    C = -(100)*(1/(cos(z)^2*cos(y/10)^2 + sin(z)^2*sin(y/10)^2))
    storage[1] =  C*(cos(z)*cos(y/10)^2*sin(z) + cos(z)*sin(z)*sin(y/10)^2)/100
    storage[2] = C*(cos(z)^2*cos(y/10)*sin(y/10) + cos(y/10)*sin(z)^2*sin(y/10))/10
end 
   
function sd_or_newton(x0::Vector,xexact::Vector,g::Function,pdir::Function,epstol::Float64,itsmax::Int64;dolinesearch=true)
    x = fill(zeros(2),itsmax)
    err = zeros(itsmax)
    err[1]=norm(x0-xexact)
    x[1] = x0;
    its = 1
    p = zeros(length(x[1]))
    while (its<itsmax)       
        pdir(x[its],p) #computes pdir(x[its]) and saves to p, overwriting it
        if dolinesearch==true
            res = optimize(s->f(x[its]+s*p),0,1)
            s = res.minimum
        else
            s = 1
        end       
        x[its+1] = x[its]+s*p 
        err[its+1] = norm(x[its+1]-xexact)
        its=its+1;
    end    
    x2 = x[1:its]
    err2 = err[1:its]
    return its, x2, err2
end
