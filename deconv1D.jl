#DECONV1D  Julia 0.3 script to plot Tikhonov and TSVD inversion for 1D example and Gaussian
#          blurring kernel 
#  
#   include("deconv1D.jl")  plots 3 figures: 1) synthetic parameter and
#                           synthetic with noise, 2) truth and Tikhonov
#                           reconstruction, 3) L-curve 
#
#   Reference:
#   Georg Stadler and Omar Ghattas,
#   Fall 2015: Computational and Variational Methods for Inverse
#   Problems,  
#   (available at
#   http://users.ices.utexas.edu/~omar/inverse_problems/notes_ch1.pdf).
#
#
#   See also DECONV2D
#

#   Omar Ghattas 2015, modified by Ellen Le 2015

using PyPlot

# Number of discretization points
# N = 128 
close("all")

N = 200 
# gamma = 0.03 
# C = 1 / (sqrt(2*pi)*gamma) 

c = 0.2

K = zeros(N,N) 
h = 1/N 
x = linspace(0,1,N) 

# discrete convolution matrix
for l = 1:N
    for k = 1:N
        ## a gaussian:
        #	K[l,k] = h * C * exp(-(l-k)^2 * h^2 / (2 * gamma^2))
        # a hat function on [-0.2,0.2] with amplitude 1/0.2 = 5 	
        K[l,k] = h * max(0,c-abs((l-k)*h))/c^2 
    end
end

# # exact parameters
# function f(y::Vector)
#     z=zeros(length(y))
#     for iter=1:length(y)
#         (y[iter] >.2 && y[iter]<.3) && (z[iter]=1)
#         y[iter] >.5 && (z[iter]=sin(4.*pi.*y[iter]))
#     end
#     return z
# end


function f(y::Vector)
    z=zeros(length(y))
    for iter=1:length(y)
        (y[iter]>.1 && y[iter]<.25) && (z[iter]=0.75)
        (y[iter]>.3 && y[iter]<.32) && (z[iter]=0.25)
        (y[iter]>.5 && y[iter]<1.0) && (z[iter]=sin(2.*pi.*y[iter]).^4)
    end
    return z
end

m = f(x)

# convolved parameters
d = K * m 

# noisy data, noise has variance = sigma^2 = 0.1
srand(0)
sigma = sqrt(0.1)
n = sigma * randn(N,1) 
dn = d + n
figure()
plot(x,d,x,dn,linewidth=2) 
legend(["data", "noisy data"]) 

# TSVD and Tikhonov regularization parameter
# alpha = 0.05 
for alpha = [1e-4,1e-3,1e-2,1e-1,1]
    #solve TSVD system 
    @assert issym(K)
    @assert isposdef(K) #throws an error if K not SPD

    U,S,() = svd(K)
    indS = find(S.^2 .>= alpha)
    Uk = U[:,indS]
    Sk = S[indS]

    m_tsvd = Uk * (diagm(1./Sk) * (Uk'*dn))

    # solve Tikhonov system
    m_tik = (K'*K + alpha * eye(N))\(K'*dn) 
    # comment out next 3 if you dont want figure
    figure()
    plot(x,m,x,m_tik,x,m_tsvd,linewidth=2)
    axis([0,1,-1.5,1.5]) 
    legend(["exact data", "Tikhonov reconstruction,
    ...alpha=$(alpha)","m_TSVD, alpha=$(alpha)"],loc="best")  
end

# plot L-curve
alpha_list = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 5e-2, 1e-1, 3e-1, 5e-1, 1, 1e1, 1e2, 1e3, 1e4] 

no = length(alpha_list) 
misfit = zeros(no)
alpha = zeros(no)              
errparam = zeros(no)
reg = zeros(no) 

for k = 1:no
    alpha = alpha_list[k]
    m_tik = (K'*K + alpha * eye(N))\(K'*dn) 
    misfit[k] = norm(K*m_tik - dn) 
    errparam[k] = norm(m - m_tik)   
    reg[k] = norm(m_tik)
end

figure()
loglog(misfit,reg,linewidth=2, marker="o",markersize=4) 
loglog(misfit[6], reg[6], "ro", linewidth=3) 
axis([misfit[1],10,10E-4,10E3]) 
xlabel("||K*m - d||"), ylabel("||m||") 
#   xlim(misfit[2],misfit[12])
#   ylim(reg[12],reg[2])

delta = norm(n)

# plot m_tik misfits to do Mozorov's

figure()
plot(1:no,misfit,linewidth=2,label="\$||Km_\\alpha-d||\$, for different \$\\alpha\$", marker="o",markersize=6)
xmin, xmax = axis()
plot((xmin, xmax), (delta, delta),"r--",linewidth=2,label="\$\\delta:=||n|| \\approx 4.7\$")
legend(loc="best")
xlabel("ith entry in \$\\alpha\$_list, in increasing order")
ylabel("\$||Km_\\alpha-d||\$")

#it apppears the 6th entry in alpha_list is the largest value of alpha that
#fits Mozorov's

@assert misfit[6] <= delta
@assert misfit[7] > delta
@show misfit[6], alpha_list[6]

# which alpha minimizes the L2 error in the parameter?
figure()
errmin = errparam[indmin(errparam)]
#plot(1,:no,log(errparam),linewidth=2,label="\$||m - m_\\alpha||\$, for different \$\\alpha\$", marker="o",markersize=6)
semilogy(1:no,errparam,linewidth=2,label="\$||m - m_\\alpha||\$, for different \$\\alpha\$", marker="o",markersize=6)
xmin,xmax = axis()
plot((xmin,xmax),(errmin,errmin),"r--",linewidth=2,label="\$||m - m_{\\alpha[6]}||\$")
legend(loc="best")
xlabel("ith entry in \$\\alpha\$_list, in increasing order")

@show indmin(errparam),alpha_list[indmin(errparam)]
