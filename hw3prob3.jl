#   HW3PROB3 Driver for Julia 0.3 code to run gradient descent and Newton for Hw 3 Problem 3
#  
#   References:
#   Essenstat and Walker, Globally Convergent Inexact Newton Methods,
#   SIAM J. Optimization, 1994
#   Kelley, Iterative Methods for Optimization, 1999
#   Nocedal and Wright, Numerical Optimization, 2nd Ed. Springer 2006
#
#   See also HW3PROB1
using PyPlot

function f3(x::Vector; sigma = 1., mu = 0.)
    A = [5 1 0 0.5; 1 4 0.5 0; 0 0.5 3 0; 0.5 0 0 2]
    Ax = A*x
    vect = x + mu*A*x
    result = 0.5*dot(x,vect) + (sigma/4)*dot(x,Ax)^2
end

function g3(x::Vector; sigma = 1., mu = 0.)
    A = [5 1 0 0.5; 1 4 0.5 0; 0 0.5 3 0; 0.5 0 0 2]
    Ax = A*x
    result = x + mu*Ax + sigma*dot(x,Ax)*Ax
end

# From p. 20 Kelley
function actionHess(g::Function, x::Vector, w::Vector, sig::Float64, m::Float64)
    h = sqrt(eps())
    if w == 0
        return 0
    else
        nw = norm(w)
        D = (g(x+h*w/nw,sigma = sig,mu = m) - g(x,sigma = sig,mu = m)) / (h/nw)
    end
end


# Inexact Newton CG p.169 Nocedal, p. 31-32 Kelley, 
function newtcg(x::Vector,xexact::Vector,f::Function,g::Function,hessvec::Function,
           sig::Float64, m::Float64; ETACASE=1, maxit=100, cgmax=100)
    itc = 1  
    err = zeros(maxit+1)
    err[itc] = norm(x-xexact)
    while (itc<=maxit)
        gc = g(x,sigma = sig,mu = m)
        nmgc = norm(gc)
        if ETACASE == 1
            eta = 0.5
        elseif ETACASE == 2
            eta = min(0.5,sqrt(nmgc))
        elseif ETACASE == 3
            eta = min(0.5,nmgc)
        end
        epstol = eta*norm(gc)
        z = 0
        r = p = gc
        d = -r
        j = 0
        while (j<cgmax)
            Hd  = hessvec(g,x,d,sig,m)
            testnd = dot(d,Hd)
            if testnd <= 0
                println("negative curvature, CG cannot continue, terminating")
                if j == 0
                    p = -gc
                    break
                else
                    p = z
                    break 
                end                   
            elseif testnd == 0
                println("warning: indefiniteness") 
            end
            alpha = dot(r,r)/dot(d,Hd)
            z = z + alpha*d
            rold = r
            r = r + alpha*Hd
            if norm(r)<epstol
                p = z
                break
            end
            beta = dot(r,r)/dot(rold,rold)
            d = -r +  beta*d
            j += 1
        end
        alpha = linesearch(f,g,p,x,sig,m)
        x = x + alpha*p       
        itc += 1
        err[itc] = norm(x-xexact)
    end    
    return x,err    
end

# Below is inexact line
function linesearch(f::Function,g::Function,p::Vector,x::Vector, sig::Float64, m::Float64; rho=1/2, c=1E-4, lsmaxit=50)
    # Backtracking line search
    # Algo 3.1 p. 37 Nocedal and Wright
    # 
    #   inputs (first 4 are required): 
    #   f -- function to minimize, outputs the val. at x
    #   g -- gradient fcn to be computed at the current iterate 
    #   p -- search direction
    #   x -- previous iterate
    #   rho -- scale down step by rho at each iteration, between (0,1), usually 1/2
    #   c -- suff. dec. parameter between 0 and 1, typically c=1E-4 in practice
    #
    #   output:
    #   alphak -- step length calculated by algorithm

    alphak = 1
    fk = f(x,sigma = sig,mu = m)
    gk = g(x,sigma = sig,mu = m)
    xc = x
    x = x + alphak*p
    fk1 = f(x,sigma = sig,mu = m) # tentative value of f_{k+1} to test
    it = 0
    while ( (fk1 > (fk + c*alphak*dot(gk,p))) && (it<lsmaxit) )
        alphak = alphak*rho
        x = xc + alphak*p
        fk1 = f(x,sigma = sig,mu = m)
        it += 1
    end
    return alphak
end


function sd(x::Vector, xexact::Vector, f::Function, g::Function, sig::Float64,
    m::Float64; maxit=100, dolinesearch=true)
    itc = 1
    err = zeros(maxit+1)
    err[itc] = norm(x-xexact)
    while (itc<=maxit)       
        p = -g(x)
        if dolinesearch==true
            s = linesearch(f,g,p,x,sig,m)
        else
            s = 1
        end       
        x = x + s*p         
        itc += 1
        err[itc] = norm(x - xexact)
    end    
    return x,err
end

xi = deg2rad(70)
x0 = vec([cos(xi) sin(xi) cos(xi) sin(xi)]')
xexact = zeros(4)

PARTCFLAG = 1
TESTFLAG = 2
if TESTFLAG == 1
    sig = 1.
    m = 0.
elseif TESTFLAG == 2
    sig = 1.
    m = 10.
end



mypath = pwd()


close("all")
ETACASE=1
if PARTCFLAG == 0
    ETACASE=1
    @time x,err = newtcg(x0,xexact,f3,g3,actionHess,sig,m,maxit=10,ETACASE=ETACASE)
    @time xsd,errsd = sd(x0,xexact,f3,g3,sig,m,maxit=10)
    plot(err, label="Newton CG w/ bt-ls", linewidth=2,marker="o",markersize=5)
    plot(errsd, label="SD w/ bt-ls", linewidth=2,marker="o",markersize=5)
    legend(loc="best")
    title("\$ \\mu \$ = $(m), \$ \\sigma \$ = $(sig), \$ \\eta \$ CASE = $(ETACASE)")
    [err errsd]
elseif PARTCFLAG == 1
    figure(1)
    figure(2)
    plt[:get_current_fig_manager]()[:window][:wm_geometry]("+0+0")
    maxn = 20
    errarray = zeros(maxn+1,3)
    figure(3,figsize=(25,6))   
    for ETACASE=1:3
        @show(ETACASE)
        @time x,err = newtcg(x0,xexact,f3,g3,actionHess,sig,m,maxit=maxn,ETACASE=ETACASE)
        lerr = log(err)
        figure(1)
        plot(6:(maxn-10),err[7:(maxn+1-10)], label="NCG, ETACASE=$(ETACASE)", linewidth=2,marker="o",markersize=5)
    
        figure(2)
        plot(err[1:end-1],err[2:end], label="NCG, ETACASE=$(ETACASE)",linewidth=2,marker="o",markersize=5)           
        figure(3)
        subplot(1,3,ETACASE)
       
        i1 = 5
        i2 = 19
       
        plot(lerr[i1:end-1],lerr[i1+1:end], label="NCG, ETACASE=$(ETACASE)",linewidth=2,marker="o",markersize=5)         
        plot(lerr[i1],lerr[i1+1], "ro", linewidth=3) 
        plot(lerr[i2],lerr[i2+1], "ro", linewidth=3)
        x1 = lerr[i2]
        p1 = lerr[i2+1]
        x2 = lerr[i1]
        p2 = lerr[i1+1]
        slope = (p2-p1)/(x2-x1)
        @show slope
        h(z) = slope.*(z-x1) + p1
        xmin,xmax = xlim()
        z = linspace(xmin,xmax,10) 
        plot(z,h(z),label="linear approxim. with slope m=$(round(slope,4))",linewidth=1,marker="o",markersize=2)
        legend(loc=2)
        title("\$ \\mu \$ = $(m), \$ \\sigma \$ = $(sig), different \$ \\eta\$ ")
        xlabel("log \$e_n\$",fontsize=15) 
        ylabel("log \$e_{n+1} \$",fontsize=15)
       
        errarray[:,ETACASE] = err
        starti = 1
        endi = 7
        if ETACASE == 2
            starti = 1
            endi = 20#10
            errrat = err[starti+1:endi]./err[starti:endi-1]
            figure()
            plot(errrat,linewidth=2,marker="o",markersize=2)
            plot(errrat[end], "ro", linewidth=3)
            ylabel("ratio \$|| x_{k+1} - x^* || / || x_k - x^* ||  \$",fontsize=15) 
            xlabel("iteration \$k\$",fontsize=15) 
            @show(errrat)
            savefig(mypath*"/superlinear.png")
        elseif ETACASE == 3
            starti = 1
            endi = 20#9
            figure()
            errratq = err[starti+1:endi] ./ ((err[starti:endi-1]).^2)
            plot(errratq,linewidth=2,marker="o",markersize=2)
            plot(errratq[end], "ro", linewidth=3)
            ylabel("ratio \$|| x_{k+1} - x^* || / || x_k - x^* ||^2  \$",fontsize=15) 
            xlabel("iteration \$k\$",fontsize=15) 
            @show(errratq)
            savefig(mypath*"/quadratic.png")
        end
    end
    figure(1)
    legend(loc="best")
    title("\$ \\mu \$ = $(m), \$ \\sigma \$ = $(sig), different \$ \\eta\$ ")
    ylabel("Error \$ || x_k - x^* || \$",fontsize=15) 
    xlabel("iteration \$k\$",fontsize=15) 
    savefig(mypath*"/q3p1.png")
    
    figure(2)
    legend(loc="best")
    title("\$ \\mu \$ = $(m), \$ \\sigma \$ = $(sig), \$ \\eta\$ CASE = $(ETACASE)  ")
    xlabel("log \$e_n\$",fontsize=15) 
    ylabel("log \$e_{n+1} \$",fontsize=15) 
    savefig(mypath*"/q3p2.png")

    figure(3)
    savefig(mypath*"/q3p1eta.png") 
    errarray   
end



# function fdcg(x::Vector,g::Function,eta::Float64,kmax::Int64,d::Vector)
#     r = -g(x)
#     rho = norm(r)^2
#     k = 1
#     d = 0
#     while ((sqrt(rho)>eta*norm(g(x))) && (k<kmax))
#         if k == 1
#             p = r        
#         else 
#             beta = rho/rhoold
#             p = r + beta*p
#             w  = actionHess(g,x,p)
#             testpw = dot(p,w)
#             if testpw < 0
#                 println("negative curvature, CG cannot continue, terminating")
#                 if 
#                 elseif testpw == 0
#                     println("warning: indefiniteness") 
#                 end
#                 alpha = rhoold/(dot(rho,w))
#                 d = d + alpha*p
#                 r = r - alpha*w
#                 rhoold = rho
#                 rho = norm(r)^2
#                 k = k + 1
#             end
#         end
#         return p
#     end

    # for j=1:6
    #     p=1 
    #     i=1
    #     a = 1
    #     while(i<5)
    #         if i == 3
    #             if a ==0
    #                 p=47
    #                 @show(p)
    #                 break
    #             else
    #                 println("works too")
    #                 p=2
    #                 break
    #             end
    #         end
    #         p = 57
    #         @show(i)
    #         @show(p)
    #         i+=1
    #     end
    #         println("out of the while loop p=$(p)")
    #         @show(j)
    #     end


# end
# end
